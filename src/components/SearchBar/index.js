import React from 'react';
import { Icon, Button, Input, AutoComplete , Row,Col } from 'antd';
import {Link , withRouter} from 'react-router-dom'
import HttpUtils from 'utils/HttpUtils'

import HotSearch from '../HotSearch'
import MiniCart from '../MinCart'

import './search.css'

const Option = AutoComplete.Option;

function renderOption(item) {
    return (
        <Option key={item.name} text={item.name}>
            {item.name}
            <span className="global-search-item-count">约 {item.relatedCount} 个结果</span>
        </Option>
    );
}

class SearchBar extends React.Component {

    state = {
        dataSource: [],
        placeholder : '巧克力'
    }

    handleSearch (value) {
        if(value.length < 3){
            return;
        }
        let self = this;
        HttpUtils.searchTips({params : {q : value}} , {
            success(resp) {
                if(resp.data){
                    self.setState({
                        dataSource: resp.data,
                    })
                }
            } ,
            error (resp) {

            }
        })
    }

    handleSelect(keyword){
        let url = HttpUtils.buildUrl("/search" , {q : keyword})
        this.props.history.push(url);
    }

    render() {
        const { dataSource } = this.state;
        return (
        <div style={{height: '120px'}}>
            <Row>
                <Col span={5}>
                    <Link to="/">
                        <img width="272" alt="logo"
                             src="https://gw.alipayobjects.com/zos/rmsportal/mqaQswcyDLcXyDKnZfES.png"/>
                    </Link>
                </Col>
                <Col span={7}>
                    <div className="global-search-wrapper" style={{marginTop:'30px'}}>
                        <AutoComplete
                            className="global-search"
                            size="large"
                            style={{ width: '100%' }}
                            dataSource={dataSource.map(renderOption)}
                            onSelect={(k) => this.handleSelect(k)}
                            onSearch={(k) => this.handleSearch(k)}
                            placeholder={this.state.placeholder}
                            optionLabelProp="text"
                        >
                            <Input
                                suffix={(
                                    <Button className="search-btn" size="large" type="primary">
                                        <Icon type="search" />
                                    </Button>
                                )}
                            />
                        </AutoComplete>
                        <div style={{marginTop : '10px'}}>
                            <HotSearch />
                        </div>
                    </div>
                </Col>
                <Col span={3} offset={1} style={{lineHeight: '100px'}}>
                <MiniCart/>
                </Col>
            </Row>
        </div>
        );
    }
}

export default withRouter(SearchBar);