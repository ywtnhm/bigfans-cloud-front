/**
 * Created by lichong on 2/14/2018.
 */

import React from 'react';
import {Row, Col, Breadcrumb, Tag, message} from 'antd';

import {withRouter} from 'react-router-dom'

import './searchfilters.css'
import HttpUtils from "utils/HttpUtils";

class SearchFilters extends React.Component {

    componentDidMount() {
        message.config({
            top: 100,
            duration: 0,
        });
        this.search()
    }

    state = {
        selectedFilters: [],
        unSelectedFilters : [],
    }

    removeSelectedFilter(removedSpec) {
        window.location.href = removedSpec.unsetUrl;
    }

    // selectCatFilter(selectedCatFilter){
    //     message.destroy();
    //     message.loading('...')
    //     selectedCatFilter.optionName='分类'
    //     this.setState({selectedCatFilter})
    //     this.search()
    //     let newCatFilterValues = this.state.catFilter.values.filter((cat) => {
    //         return cat.valueId !== selectedCatFilter.valueId;
    //     })
    //     let catFilter = this.state.catFilter;
    //     catFilter.values = newCatFilterValues;
    //     this.setState({catFilter:catFilter});
    // }

    // selectFilter(selectedFilter) {
    //     message.destroy();
    //     message.loading('...')
    //     let currentFilters = this.state.attrFilters;
    //     let selectedFilters = this.state.selectedFilters;
    //     let newSelectedFilter;
    //     currentFilters.map((filter) => {
    //         filter.values.filter((val) => {
    //             let rm = val.id === selectedFilter.id;
    //             if (rm) {
    //                 newSelectedFilter = {
    //                     optionId: filter.optionId,
    //                     valueId : val.valueId,
    //                     name: filter.optionName + " : " + val.value
    //                 }
    //             }
    //             return rm;
    //         })
    //     })
    //     let exists = selectedFilters.some((spec) => {
    //         return (spec.optionId === newSelectedFilter.id && spec.valueId === newSelectedFilter.valueId)
    //     })
    //     if (!exists) {
    //         selectedFilters.push(newSelectedFilter);
    //         this.setState({selectedFilters: selectedFilters});
    //     }
    //     this.search();
    // }

    search() {
        // let params = {};
        // let q = HttpUtils.getQueryString("q");
        // let catId = HttpUtils.getQueryString("catId");
        // let filters = this.state.selectedFilters.map((f) => {
        //     return f.optionId + '_' + f.valueId;
        // }).join('&');
        // let orderBy = HttpUtils.getQueryString("orderBy");
        // let cp = HttpUtils.getQueryString("cp");
        // if(q){
        //     params.q = q;
        // }
        // if(catId){
        //     params.catId = catId;
        // }
        // if(filters){
        //     params.filters = filters;
        // }
        // if(orderBy){
        //     params.orderBy = orderBy
        // }
        // if(cp){
        //     params.cp = cp;
        // }
        // this.props.history.push(HttpUtils.buildUrl('/search' + window.location.search , {}));
        let self = this;
        HttpUtils.searchProducts(window.location.search , {
            success(resp){
                self.setState(resp.data)
                self.props.setSearchResult(resp.data);
                message.destroy();
            } ,
            error (resp){
                message.destroy();
            }
        })
    }

    render() {
        return (
            <div>
                <Row>
                    <Col span={2}>
                        <Breadcrumb separator=">">
                            <Breadcrumb.Item>全部结果></Breadcrumb.Item>
                        </Breadcrumb>
                    </Col>
                    {
                        this.state.selectedFilters.map((selectedFilter) => {
                            return (
                                <Tag key={selectedFilter.optionId}
                                     closable
                                     onClose={() => this.removeSelectedFilter(selectedFilter)}>
                                    {selectedFilter.label}
                                </Tag>
                            )
                        })
                    }
                </Row>
                <div className="search-filters">
                    {
                        this.state.unSelectedFilters.map((filter , index) => {
                            return (
                                <Row key={index} className='spec-group'>
                                    <Col span={3} className='spec-option'>
                                        <span>{filter.label}:</span>
                                    </Col>
                                    <Col span={21}>
                                        {
                                            filter.items.map((sv , index) => {
                                                return (
                                                    <a
                                                       className='spec-value'
                                                       href={'/search' + sv.url}
                                                       key={index}>
                                                        {sv.label}
                                                    </a>
                                                )
                                            })
                                        }
                                    </Col>
                                </Row>
                            )
                        })
                    }
                </div>
            </div>
        );
    }
}

export default withRouter(SearchFilters);